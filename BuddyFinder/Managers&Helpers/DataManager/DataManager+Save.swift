import Foundation
import RealmSwift
import Alamofire
import ObjectMapper

extension DataManager {
        
    func saveVenuesDataToRealm(_ venuesArray : [VenueModel]) -> [VenueModel] {
        
        let realm = RealmProvider.realm()
        do {
            try realm?.write {
                for venue in venuesArray{
                    realm?.add(venue, update: true)
                }
            }
        } catch  {
            return [VenueModel]()
        }
        
        return venuesArray
    }
    
}
