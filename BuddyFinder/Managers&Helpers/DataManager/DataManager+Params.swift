import Foundation
import RealmSwift
import Alamofire
import ObjectMapper

extension DataManager{

    func parametersForVenues() -> [String:Any] {
        let params = ["ll": FourSquare.ll, "client_id" : FourSquare.client_id, "client_secret": FourSquare.client_secret, "v":Date().string(formatted: "YYYYMMDD")!, "categoryId": FourSquare.resturantCategoryID, "radius" : FourSquare.radius, "limit" : FourSquare.limit] as [String : Any]
        return params
    }

}
