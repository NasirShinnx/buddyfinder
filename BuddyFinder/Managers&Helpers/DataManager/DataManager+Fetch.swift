import Foundation
import RealmSwift
import Alamofire
import ObjectMapper

extension DataManager {
    
    func fetchVenuesData(_ completion: @escaping (_ results: [VenueModel]?, _ error: DataError?) -> Void){
        
        if let realm = RealmProvider.realm(){
            let venues = (realm.objects(VenueModel.self).toArray())
            if !venues.isEmpty {
                completion(venues, nil)
            }
            else{
                FetchVenue() {(venuesArray : [VenueModel]) in
                    completion(venuesArray, nil)
                    }.execute(parametersForVenues())
            }
        }
        else{
            print("error")
        }
    }
}
