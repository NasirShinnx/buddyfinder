/*
 
 NASIR MAHMOOD
 
 Comment:
 Work in combination with  Network manager and Response to handle types of data
 Network
 
 */
import Foundation
import Alamofire
import AlamofireObjectMapper


class FetchVenue : Command {
    
    var completionHandler: ([VenueModel]) -> Void
    
    required init(completionHandler: @escaping ([VenueModel])->Void) {
        self.completionHandler = completionHandler
    }
    
    public func execute(_ params : [String:Any]) {
            NetworkManager.sharedInstance.request("\(FourSquare.basicURLString)\(APIEndPointType.venues.rawValue)", method: .get, parameters: params).responseObject { (response: DataResponse<VenuesResponse>) in
                guard let venueresponse = response.value else {
                    return
                }
                self.completionHandler(DataManager.sharedInstance.saveVenuesDataToRealm(venueresponse.venueResponse.venues))
        }
    }
}
