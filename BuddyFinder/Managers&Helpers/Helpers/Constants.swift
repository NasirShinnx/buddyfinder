/*
 
 NASIR MAHMOOD
 
 Comment:
 Constants
 All the constants will be here
 
 */

import Foundation

let basicURL            = "https://api.foursquare.com/v2/"


struct FourSquare {
    static let basicURLString        = "https://api.foursquare.com/v2/"
    static let client_id             = "ECUFCSQPRL5X2VOL0KCN5C02SQ30FSRH4SHKUFJ1Y3ZF4SE0"
    static let client_secret         = "EVKU4PVHGPYLEGCEK3JSSHSTGHMEUXN3RXHRKZTUZLGFBXFY"
    static let version               = "20121105"
    static let ll                    = "25.0886030,55.1480780" // hardcoding Marina DXB Location for testability
    static let resturantCategoryID   = "4d4b7105d754a06374d81259"
    static let radius                = 3000
    static let limit                 = 10

}

// Add all general error messages here
struct ErrorMessage {

}


// Realm Environments
struct RealmInMemoryEnvironments {
    static let TEST                 = "test"
    static let APPLICATION          = "Venues"
    
}

// Viewcontroller Titles
struct ViewControllerTitles {
    static let Venues                = "Venues List"

}

// Cell Identifiers
struct CellIdentifiers {
    static let Venues                = "VenuesTableViewCustomCell"
    static let LocationPin           = "Location"

    
}

// API Endpoints
enum APIEndPointType: String {
    case venues                      = "venues/search?"
    
}

// Errors Struct
enum DataError: Error {
    case missing(String)
    case invalid(String, Any)
}








