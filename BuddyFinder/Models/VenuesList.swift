/*
 
 NASIR MAHMOOD
 
 Comment:
 MODEL OBJECT
 Made with Object MApper to automap objects.
 
 */

import Foundation
import ObjectMapper

fileprivate struct JSONConstants {
    static let kResponse       = "response"
    static let kResponseVenues = "venues"

}

class VenuesResponse: Mappable {
    
    var venueResponse = VenuesList()
    // all others like meta can be added here
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        venueResponse <- map[JSONConstants.kResponse]
    }
}

class VenuesList: Mappable {
    var venues    = [VenueModel]()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        venues <- map[JSONConstants.kResponseVenues]
    }
}
