/*
 
 NASIR MAHMOOD
 
 Comment:
 MODEL OBJECT
 Made with Realm and Object MApper to automap objects to RealmDatabase.
 
 */

import Foundation
import RealmSwift
import ObjectMapper

fileprivate struct JSONConstants {
    
    static let kID        = "id"
    static let kName      = "name"
    static let kVerified  = "verified"
    static let kUrl       = "url"
    static let kHasPerk   = "hasPerk"
    static let kLocation  = "location"
    
}

class VenueModel: Object, Mappable {
    
    dynamic var id        =  ""
    dynamic var name      =  ""
    dynamic var url       =  ""
    dynamic var website   =  ""
    
    var verified          =  RealmOptional<Bool>()
    var hasPerk           =  RealmOptional<Bool>()

    
    dynamic var location : AddressModel?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func completeAddress() -> String {
        return "\((location?.address)!) \((location?.crossStreet)!) \((location?.city)!) \((location?.country)!)"
    }
}

//MARK: Mapper EXtension
extension VenueModel
{
    func mapping(map: Map) {
        id             <- map[JSONConstants.kID]
        name           <- map[JSONConstants.kName]
        verified.value <- map[JSONConstants.kVerified]
        url            <- map[JSONConstants.kUrl]
        hasPerk.value  <- map[JSONConstants.kHasPerk]
        location       <- map[JSONConstants.kLocation]
    }

}
