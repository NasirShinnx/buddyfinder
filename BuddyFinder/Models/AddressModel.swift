/*
 
 NASIR MAHMOOD
 
 Comment:
 MODEL OBJECT
 Made with Realm and Object MApper to automap objects to RealmDatabase.
 
 */

import Foundation
import RealmSwift
import ObjectMapper

fileprivate struct JSONAddressConstants {
    
    static let kAddress        = "address"
    static let kcrossStreet    = "crossStreet"
    static let kCity           = "city"
    static let kState          = "state"
    static let kCountry        = "country"
    static let kLat            = "lat"
    static let kLon            = "lng"
}

class AddressModel: Object, Mappable {
    
    dynamic var address        =  ""
    dynamic var crossStreet    =  ""
    dynamic var city           =  ""
    dynamic var state          =  ""
    dynamic var country        =  ""
    
    var lat =  RealmOptional<Double>()
    var lng =  RealmOptional<Double>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
}

//MARK: Mapper EXtension
extension AddressModel
{
    func mapping(map: Map) {
        
        address       <- map[JSONAddressConstants.kAddress]
        crossStreet   <- map[JSONAddressConstants.kcrossStreet]
        city          <- map[JSONAddressConstants.kCity]
        state         <- map[JSONAddressConstants.kState]
        country       <- map[JSONAddressConstants.kCountry]
        
        lat.value     <- map[JSONAddressConstants.kLat]
        lng.value     <- map[JSONAddressConstants.kLon]


    }

}
