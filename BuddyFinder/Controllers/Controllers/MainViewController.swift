/*
 
 NASIR MAHMOOD
 
 Comment:
 View Controller
 
 */
import UIKit

@objc class MainViewController: UIViewController {
    
    @IBOutlet var segmentedControl: UISegmentedControl!
    var mapViewController : MapViewController?
    let topSpacing = 109 // Statusbar+NavBar+Spacing+Segment+Spacing
    
    private lazy var _venuesViewController: VenuesViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "VenuesViewController") as! VenuesViewController
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var _mapViewController: MapViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title="Resturants"
        setupView()
    }
    
    // MARK: - View Methods
    private func setupView() {
        setupSegmentedControl()
        updateView()
    }
    
    private func updateView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: _mapViewController)
            add(asChildViewController: _venuesViewController)
        } else {
            remove(asChildViewController: _venuesViewController)
            add(asChildViewController: _mapViewController)
        }
    }
    
    private func setupSegmentedControl() {
        // Configure Segmented Control
        segmentedControl.removeAllSegments()
        segmentedControl.insertSegment(withTitle: "List View", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "Map View", at: 1, animated: false)
        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 0
    }
    
    // MARK: - Actions
    func selectionDidChange(_ sender: UISegmentedControl) {
        updateView()
    }
    
    // MARK: - Helper Methods
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        view.addSubview(viewController.view)
        let frame : CGRect = CGRect(x:0, y:109, width:view.bounds.size.width, height:view.bounds.size.height-109)
        
        viewController.view.frame = frame//view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
}
