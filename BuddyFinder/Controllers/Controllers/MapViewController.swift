/*
 
 NASIR MAHMOOD
 
 Comment:
 View Controller
 
 */

import Foundation
import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    var venuesArray : [VenueModel] = [] // needed in tests
    var annotations : [VenueAnnotation] = [] // needed in tests
    
    let height	=	53.0
    let width	=	52.0
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataManager.sharedInstance.fetchVenuesData() { (venuesArray, error) in
            if let venuesUnwrapped=venuesArray{
                self.venuesArray=venuesUnwrapped
                OperationQueue.main.addOperation {
                    self.activityIndicator.stopAnimating()
                    self.addPinsForVenue()
                }
            }
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// Mark: Custom Private Methods
extension MapViewController {
    
    fileprivate func addPinsForVenue(){
        
        for venueObj in self.venuesArray {
            let locationCoordinates = CLLocationCoordinate2D(latitude: (venueObj.location?.lat.value!)!, longitude: (venueObj.location?.lng.value!)!)
            let annotation = VenueAnnotation()
            annotation.coordinate = locationCoordinates
            annotation.imageName  = "resturant_pin_icon"
            annotation.title = venueObj.name
            annotations.append(annotation)
            
        }
        
        mapView.addAnnotations(annotations)
        mapView.showAnnotations(annotations, animated: true)
    }
    
    
}

//MArk: Mapkitview
extension MapViewController
{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is VenueAnnotation) {
            return nil
        }
        
        let venueAnn = annotation as! VenueAnnotation
        let annotationView = MKAnnotationView(annotation: venueAnn, reuseIdentifier: CellIdentifiers.LocationPin)
        annotationView.frame = CGRect(x: 0, y: 0, width: width*1.5, height: height*1.8)
        annotationView.centerOffset = CGPoint(x: 0.0, y: -height/2)
        annotationView.isEnabled=true
        annotationView.canShowCallout=true
        annotationView.image = UIImage(named: venueAnn.imageName!)
        
        return annotationView
    }
    
}
