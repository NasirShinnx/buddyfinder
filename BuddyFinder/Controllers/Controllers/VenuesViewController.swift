/*
 
 NASIR MAHMOOD
 
 Comment:
 View Controller
 
 */
import UIKit
import RealmSwift
import CoreLocation

class VenuesViewController: UIViewController {
    
    /*fileprivate*/ var venuesArray : [VenueModel] = [] // needed in tests
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var tableViewEstimatedHight = Float?(150.0)
    var tableViewCellIdentifier = ""
    
    let numberOfSections = 1
    
    //MARK: UIView Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = CGFloat(tableViewEstimatedHight!)
        tableView.rowHeight = UITableViewAutomaticDimension;
        
        activityIndicator.startAnimating()
        DataManager.sharedInstance.fetchVenuesData() { (venuesArray, error) in
            if let venuesUnwrapped=venuesArray{
                self.venuesArray=venuesUnwrapped
                OperationQueue.main.addOperation {
                    self.activityIndicator.stopAnimating()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension VenuesViewController : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venuesArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.Venues)! as! VenuesTableViewCustomCell
        if !venuesArray.isEmpty {
            let venue = venuesArray[indexPath.row]
            cell.populateReceiepeObject(venue)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
