/*
 
 NASIR MAHMOOD
 
 Comment:
 Extension on Date OBJECT
 
 */
import Foundation

extension Date{
    func string(formatted: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatted
        dateFormatter.locale = Locale(identifier: "en")
        return dateFormatter.string(from: self)
    }
}
