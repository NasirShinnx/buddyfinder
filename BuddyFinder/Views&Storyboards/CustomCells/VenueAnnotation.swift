import Foundation
import UIKit
import MapKit
import CoreLocation

class VenueAnnotation: MKPointAnnotation {
    var imageName: String?
}
