import Foundation
import UIKit

class VenuesTableViewCustomCell: UITableViewCell {
    
    @IBOutlet var nameLabel         : UILabel!
    @IBOutlet var venueNameLabel    : UILabel!
    @IBOutlet var urlLabel          : UILabel!
    @IBOutlet var fullAddressLabel  : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    open func populateReceiepeObject(_ venueObj : VenueModel)  {
        nameLabel.text           = venueObj.name
        venueNameLabel.text      = "\((venueObj.location?.address)!) Branch"
        urlLabel.text            = venueObj.url
        fullAddressLabel.text    = venueObj.completeAddress()
    }
    
}
