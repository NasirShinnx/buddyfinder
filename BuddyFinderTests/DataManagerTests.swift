import XCTest
@testable import BuddyFinder

class DataManagerTests: XCTestCase {
    
    var dataManager : DataManager?
    
    override func setUp() {
        super.setUp()
        dataManager = DataManager()

        let realm = RealmProvider.realm()
        try! realm?.write { () -> Void in
            realm?.deleteAll()
        }
        // add dummy data to database
    }
    
    func testVenuesFetch() {
        
        let exceptionDesc: XCTestExpectation = self.expectation(description: "exception")
        
        dataManager?.fetchVenuesData({ (venuesArray, dataError) in
            // 1. Non empty result
            XCTAssertTrue(venuesArray?.count == 10)
            
            // Not Nil
            XCTAssertNotNil(venuesArray?[0])
            
            // first object Data testing
            let firstObject = venuesArray?[0]
            
            XCTAssertEqual(firstObject?.id, "4f984168e4b04aec2d0a2348")
            
            XCTAssertEqual(firstObject?.url, "http://www.reemalbawadi.com")
            
            XCTAssertEqual(firstObject?.location?.city, "Dubai")
            
            XCTAssertEqual(firstObject?.location?.address, "Marina Walk")
            
            exceptionDesc.fulfill()
        })
        
        
        self.waitForExpectations(timeout: 100) { (error) in
            XCTAssertNil(error, "Some error occured")
        }
    }
    
}
