import XCTest
@testable import BuddyFinder

class VenuesViewControllerTests: XCTestCase {

    var venuesViewController: VenuesViewController!
    
    override func setUp() {
        
        super.setUp()
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        venuesViewController = storyboard.instantiateViewController(withIdentifier: "VenuesViewController") as! VenuesViewController
        _ = venuesViewController.view
    }
    
    func testTableViewIsNotNilAfterViewDidLoad() {
        XCTAssertNotNil(venuesViewController.tableView)
    }
    
    func testShouldSetTableViewDataSource() {
        XCTAssertNotNil(venuesViewController.tableView.dataSource)
    }
    
    func testShouldSetTableViewDelegate() {
        XCTAssertNotNil(venuesViewController.tableView.dataSource)
    }
    
    func testConformsToTableViewDataSourceProtocol() {
        
        XCTAssert(venuesViewController.conforms(to: UITableViewDataSource.self))
        
        XCTAssert(venuesViewController.responds(to: #selector(venuesViewController.numberOfSections(in:))))
        
        XCTAssert(venuesViewController.responds(to: #selector(venuesViewController.tableView(_:numberOfRowsInSection:))))
        
        XCTAssert(venuesViewController.responds(to: #selector(venuesViewController.tableView(_:cellForRowAt:))))
    }
    
    func testTableViewUsesCustomCell_RecepiesTableViewCustomCell() {
        
         let firstCell = venuesViewController.tableView(venuesViewController.tableView, cellForRowAt: IndexPath.init(row: 0, section: 0))
        
        XCTAssert(firstCell is VenuesTableViewCustomCell)
    }
    
    func testConformsToTableViewDelegateProtocol() {
        
        XCTAssert(venuesViewController.conforms(to: UITableViewDelegate.self))
        
        XCTAssert(venuesViewController.responds(to: #selector(venuesViewController.tableView(_:didSelectRowAt:))))
    }
    
}
